using ArchUnitNET.Domain;
using ArchUnitNET.Loader;
using ArchUnitNET.Fluent;
using ArchUnitNET.xUnit;
using Xunit;

//add a using directive to ArchUnitNET.Fluent.ArchRuleDefinition to easily define ArchRules
using static ArchUnitNET.Fluent.ArchRuleDefinition;

namespace MyVod.ArchTests;

public class UnitTest1
{
    private static readonly Architecture Architecture = new ArchLoader().LoadAssemblies(
        System.Reflection.Assembly.Load("MyVod.Domain"),
        System.Reflection.Assembly.Load("MyVod.Infrastructure"))
            .Build();
    
    private readonly IObjectProvider<IType> ExampleLayer =
        Types().That().ResideInAssembly("MyVod.Domain").As("Domain Layer");

    private readonly IObjectProvider<IType> ForbiddenLayer =
        Types().That().ResideInNamespace("MyVod.Infrastructure").As("Infrastructure Layer");
    
    [Fact]
    public void Domain_Layer_Should_Not_Reach_Infrastructure()
    {
        IArchRule exampleLayerShouldNotAccessForbiddenLayer = Types().That().Are(ExampleLayer).Should()
            .NotDependOnAny(ForbiddenLayer).Because("it's forbidden");
        exampleLayerShouldNotAccessForbiddenLayer.Check(Architecture);
    }
}