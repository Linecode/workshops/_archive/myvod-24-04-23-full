using MongoDB.Driver;
using MyVod.Domain.Invoicing.Domain;
using MyVod.Domain.Invoicing.Infrastructure;

namespace MyVod.Infrastructure.Invoicing;

public class InvoiceRepository : IInvoiceRepository
{
    private IMongoDatabase Database { get; }
    private const string CollectionName = "invoices";

    public InvoiceRepository(IMongoClient mongoClient)
    {
        Database = mongoClient.GetDatabase("invoices");
    }

    public async Task Add(Invoice invoice)
    {
        var collection = Database.GetCollection<Invoice>(CollectionName);

        await collection.InsertOneAsync(invoice);
    }

    public async Task<Invoice> Get(InvoiceId id)
    {
        var collection = Database.GetCollection<Invoice>(CollectionName);

        var result = await collection.Find(x => x.Id.Equals(id)).Limit(1).ToListAsync();

        return result.SingleOrDefault()!;
    }
}