using System.ComponentModel.DataAnnotations;
using MediatR;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Domain;

public sealed class License : Entity<LicenseId>, AggregateRoot<LicenseId>
{
    public DateRange DateRange { get; private set; } = null!;
    public RegionIdentifier RegionIdentifier { get; private set; } = null!;

    public MovieId MovieId { get; private set; }

    public LicenseStatus Status { get; private set; } = LicenseStatus.NotActive;   

    [Obsolete("Only For EF", true)]
    private License()
    {
    }
    
    public License(DateRange dateRange, RegionIdentifier regionIdentifier, MovieId movieId)
    {
        DateRange = dateRange;
        RegionIdentifier = regionIdentifier;
        MovieId = movieId;
        
        Id = LicenseId.New();
    }

    public void Activate()
        => Status = LicenseStatus.Active;

    public void DeActivate()
        => Status = LicenseStatus.NotActive;

    public enum LicenseStatus
    {
        NotActive,
        Active
    }
}
