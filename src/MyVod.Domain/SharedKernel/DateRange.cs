using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.SharedKernel;

public class DateRange : ValueObject<DateRange>
{
    public DateTime StartDate { get; private set; }

    public DateTime EndDate { get; private set; }
    
    [Obsolete("Only for EF", true)]
    private DateRange() { }

    private DateRange(DateTime startDate, DateTime? endDate = null)
    {
        StartDate = startDate;
        EndDate = endDate ?? DateTime.MaxValue;
    }

    public static DateRange Create
    (
        DateTime startDate,
        DateTime? endDate = null
    )
    {
        if (endDate.HasValue)
        {
            if (endDate.Value < startDate)
            {
                throw new ArgumentOutOfRangeException("The end date cannot be before the start date.");
            }
        }

        var range = new DateRange(startDate, endDate);

        return range;
    }

    public bool IsWithinRange(DateTime date)
    {
        return date >= StartDate && date <= EndDate;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return StartDate;
        yield return EndDate;
    }

    public override string ToString()
    {
        var startDisplay = FormatDate(StartDate);
        var endDisplay = FormatDate(EndDate);

        string FormatDate(DateTime date)
        {
            return date.ToShortDateString();
        }

        return $"{startDisplay} to {endDisplay}";
    }
}

public record ParamsObject(bool Flag1, bool Flag2);

public record CategoryId(int Value);
public record ProductId(int Value);

public record Height(int Value);
public record Width(int Value);

public record Dimension(int Value);

public class Service
{
    public void Method(ParamsObject @params)
    {
        var categoryId = new CategoryId(1);
        var productId = new ProductId(2);

        SecondMethod(categoryId, productId);
    }

    public void SecondMethod(CategoryId categoryId, ProductId productId)
    {
        
    }

    public void CalculateRectArea(Dimension height, Dimension width)
    {
        
    }
}