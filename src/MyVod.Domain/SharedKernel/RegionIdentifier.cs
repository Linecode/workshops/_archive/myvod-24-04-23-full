using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.SharedKernel;

// ISO 3166
public class RegionIdentifier : ValueObject<RegionIdentifier>
{
    public ushort Value { get; private set; } = ushort.MinValue;

    [Obsolete("Only for EF", true)]
    private RegionIdentifier()
    {
    }
    
    public RegionIdentifier(ushort id)
    {
        Value = id;
    }

    public override string ToString()
        => Value.ToString().PadLeft(3, '0');

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }

    public static readonly RegionIdentifier Poland = new(616);
    // ReSharper disable once InconsistentNaming
    public static readonly RegionIdentifier USA = new(840);
    public static readonly RegionIdentifier Missing = new(000);
    public static readonly RegionIdentifier All = new(999);
}

public class GeoPoint : ValueObject<GeoPoint>
{
    public double X { get; private set; }
    public double Y { get; private set; }

    private GeoPoint(double x, double y)
    {
        X = x;
        Y = y;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return X;
        yield return Y;
    }

    public static GeoPoint New(double x, double y)
    {
        Ensure.That(y).IsInRange(-180, 180);
        Ensure.That(x).IsInRange(-90, 90);
        
        return new GeoPoint(x, y);
    }
}
