using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Domain;

public sealed class Movie : Entity<MovieId>, AggregateRoot<MovieId>
{
    public Title Title { get; private set; } = null!;
    public Description Description { get; private set; } = null!;

    public MetaData MetaData { get; private set; } = null!;

    public Uri Cover { get; private set; } = null!;
    public Uri Trailer { get; private set; } = null!;

    public Director Director { get; private set; } = null!;

    public MovieStatus Status { get; private set; } = MovieStatus.UnPublished;

    public byte[] Timestamp { get; private set; } = null!;

    public RegionIdentifier RegionIdentifier { get; private set; } = null!;

    public Money Price { get; private set; } = null!;

    [Obsolete("Only For EF", true)]
    private Movie()
    { }

    internal Movie(MovieId id)
    {
        Id = id;
    }

    public Movie(Title title, Description description)
    {
        Title = title;
        Description = description;
        
        Id = MovieId.New();
    }

    public void AddMetaData(MetaData metaData)
    {
        MetaData = metaData;
    }

    public void DefineMedia(Uri cover, Uri trailer)
    {
        Ensure.That(cover).IsNotNull();
        Ensure.That(trailer).IsNotNull();
        
        Cover = cover;
        Trailer = trailer;
    }

    public void Publish(RegionIdentifier regionIdentifier, PublishPolicy policy)
    {
        var result = policy.Publish(this);

        if (result.IsSuccessful)
        {
            Status = MovieStatus.Published;
            RegionIdentifier = regionIdentifier;
        }
    }

    public void UnPublish()
    {
        Status = MovieStatus.UnPublished;
    }

    public bool HasPrice => Price != null!;

    public enum MovieStatus
    {
        UnPublished,
        Published
    }
}

public class InvoiceService
{
    public void Display()
    {
        
    }
}

public class InvoiceHeader : Entity<Guid>, AggregateRoot<Guid>
{
    public DateTime CreatedAt { get; private set; }
    public InvoiceParty From { get; private set; }
    public InvoiceParty To { get; private set; }

    public InvoiceHeader(DateTime createdAt, InvoiceParty from, InvoiceParty to)
    {
        CreatedAt = createdAt;
        From = from;
        To = to;
    }
}


public sealed class Invoice : Entity<Guid>, AggregateRoot<Guid>
{
    public List<InvoiceLine> Lines { get; private set; } = new();

    public Invoice(List<InvoiceLine> lines)
    {
        Lines = lines;
        
        Id = Guid.NewGuid();
    }

    public int Calculate()
    {
        return Lines.Select(x => x.Calculate()).Sum();
    }

    public static Invoice New(List<InvoiceLine> lines)
    {
        return new Invoice(lines);
    }
}

public class InvoiceParty 
{

}

public class InvoiceLine
{
    public int Price { get; private set; }
    public int Quantity { get; private set; }
    public int TaxRate { get; private set; }

    public int Calculate()
        => Price * Quantity * TaxRate;
}































