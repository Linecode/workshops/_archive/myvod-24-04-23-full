using System.Runtime.Serialization;

namespace MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;

public class MissingPriceException : Exception
{
    public MissingPriceException()
    {
    }

    protected MissingPriceException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public MissingPriceException(string? message) : base(message)
    {
    }

    public MissingPriceException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}