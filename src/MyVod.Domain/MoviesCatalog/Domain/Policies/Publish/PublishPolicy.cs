using MyVod.Common.BuildingBlocks.Common;

namespace MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;

// ReSharper disable once InconsistentNaming
public interface PublishPolicy
{
    Result Publish(Movie movie);
}