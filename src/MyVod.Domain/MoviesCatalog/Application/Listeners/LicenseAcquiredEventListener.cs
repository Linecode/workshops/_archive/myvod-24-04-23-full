using MediatR;
using MyVod.Domain.CannonicalModel.Events;

namespace MyVod.Domain.MoviesCatalog.Application.Listeners;

public class LicenseAcquiredEventListener : INotificationHandler<LicenseAcquiredEvent>
{
    private readonly IMovieService _movieService;

    public LicenseAcquiredEventListener(IMovieService movieService)
    {
        _movieService = movieService;
    }
    
    public async Task Handle(LicenseAcquiredEvent notification, CancellationToken cancellationToken)
    {
        await _movieService.Publish(notification.MovieId, notification.RegionIdentifier);
    }
}