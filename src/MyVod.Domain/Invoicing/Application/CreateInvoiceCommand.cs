using MediatR;
using MyVod.Domain.Invoicing.Domain;
using MyVod.Domain.Invoicing.Infrastructure;

namespace MyVod.Domain.Invoicing.Application;

public record CreateInvoiceCommand(DateTime DueDate, IEnumerable<InvoiceLine> Lines, InvoiceParty From, InvoiceParty To) : IRequest;

public class CreateInvoiceHandler : IRequestHandler<CreateInvoiceCommand>
{
    private readonly IInvoiceRepository _repository;

    public CreateInvoiceHandler(IInvoiceRepository repository)
    {
        _repository = repository;
    }
    
    public async Task Handle(CreateInvoiceCommand request, CancellationToken cancellationToken)
    {
        var invoice = new Invoice(request.From, request.To, request.DueDate, request.Lines);

        await _repository.Add(invoice);
    }
}