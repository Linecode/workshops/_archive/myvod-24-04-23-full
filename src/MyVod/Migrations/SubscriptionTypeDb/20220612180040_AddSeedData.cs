﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyVod.Migrations.SubscriptionTypeDb
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SubscriptionTypes",
                columns: new[] { "Id", "Price", "TimeSpan" },
                values: new object[] { new Guid("37dc0eaf-3a34-49a2-8208-e67539fd078e"), 80000u, new TimeSpan(360, 0, 0, 0, 0) });

            migrationBuilder.InsertData(
                table: "SubscriptionTypes",
                columns: new[] { "Id", "Price", "TimeSpan" },
                values: new object[] { new Guid("4bcb5575-a3b0-48cd-9199-beec567433cc"), 10000u, new TimeSpan(30, 0, 0, 0, 0) });

            migrationBuilder.InsertData(
                table: "SubscriptionTypes",
                columns: new[] { "Id", "Price", "TimeSpan" },
                values: new object[] { new Guid("9abd91da-9cac-4b4b-9a8f-2ecb3fd01090"), 55000u, new TimeSpan(180, 0, 0, 0, 0) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SubscriptionTypes",
                keyColumn: "Id",
                keyValue: new Guid("37dc0eaf-3a34-49a2-8208-e67539fd078e"));

            migrationBuilder.DeleteData(
                table: "SubscriptionTypes",
                keyColumn: "Id",
                keyValue: new Guid("4bcb5575-a3b0-48cd-9199-beec567433cc"));

            migrationBuilder.DeleteData(
                table: "SubscriptionTypes",
                keyColumn: "Id",
                keyValue: new Guid("9abd91da-9cac-4b4b-9a8f-2ecb3fd01090"));
        }
    }
}
