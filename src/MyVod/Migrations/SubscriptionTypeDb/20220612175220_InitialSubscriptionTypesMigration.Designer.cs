﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyVod.Infrastructure.Marketing;

#nullable disable

namespace MyVod.Migrations.SubscriptionTypeDb
{
    [DbContext(typeof(SubscriptionTypeDbContext))]
    [Migration("20220612175220_InitialSubscriptionTypesMigration")]
    partial class InitialSubscriptionTypesMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.5");

            modelBuilder.Entity("MyVod.Domain.Marketing.Domain.SubscriptionType", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<uint>("Price")
                        .HasColumnType("INTEGER");

                    b.Property<TimeSpan>("TimeSpan")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SubscriptionTypes");
                });
#pragma warning restore 612, 618
        }
    }
}
