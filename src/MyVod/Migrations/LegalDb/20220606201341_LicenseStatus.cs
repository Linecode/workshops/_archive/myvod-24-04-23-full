﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyVod.Migrations.LegalDb
{
    public partial class LicenseStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Licenses",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Licenses");
        }
    }
}
