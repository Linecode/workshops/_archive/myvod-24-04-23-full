using MyVod.Infrastructure.Models;

namespace MyVod.Models;

public class CheckoutViewModel
{
    public Movie Movie { get; set; }
    public int Tax { get; set; }
}